module StatusBar where

import XMonad
import XMonad.Actions.CopyWindow
import XMonad.Hooks.StatusBar
import qualified XMonad.Hooks.StatusBar as SB
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.UrgencyHook
import qualified XMonad.StackSet as W
import XMonad.Util.NamedWindows
import XMonad.Util.Run

-----------------------------------------------------------------------
--
-----------------------------------------------------------------------
data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
  urgencyHook LibNotifyUrgencyHook w = do
    name <- getName w
    Just idx <- fmap (W.findTag w) $ gets windowset

    safeSpawn "notify-send" [show name, "workspace " ++ idx]

-----------------------------------------------------------------------
--
-----------------------------------------------------------------------
myPP :: PP
myPP =
  def
    { ppCurrent = xmobarColor "#643FFF" "" . \s -> "<fn=2>\63202</fn>",
      ppHidden = xmobarColor "#F28FAD" "" . \s -> "<fn=2>\63255</fn>",
      ppHiddenNoWindows = xmobarColor "#00ffd0" "" . \s -> "<fn=4>\61713</fn>",
      ppUrgent = xmobarColor "#74c7ec" "" . \s -> "<fn=2>\63189</fn>",
      ppTitle = xmobarColor "#F28FAD" "" . \s -> "",
      ppSep = "  ",
      ppOrder = \[ws, l, t] -> [l, ws, t]
    }

myPP' = copiesPP (xmobarColor "#f9e2af" "" . \s -> "<fn=2>\63099</fn>") myPP

mySB :: FilePath -> SB.StatusBarConfig
mySB xmobarPath =
  (SB.statusBarProp xmobarPath myPP')
    { SB.sbCleanupHook = SB.killAllStatusBars,
      SB.sbStartupHook = SB.killAllStatusBars >> SB.spawnStatusBar xmobarPath
    }
